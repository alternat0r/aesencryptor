﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnProc = New System.Windows.Forms.Button()
        Me.txtPlain = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEnc = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDec = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.txtKEY = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnCopyEnc = New System.Windows.Forms.Button()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnProc
        '
        Me.btnProc.Location = New System.Drawing.Point(123, 142)
        Me.btnProc.Name = "btnProc"
        Me.btnProc.Size = New System.Drawing.Size(128, 39)
        Me.btnProc.TabIndex = 0
        Me.btnProc.Text = "Process"
        Me.btnProc.UseVisualStyleBackColor = True
        '
        'txtPlain
        '
        Me.txtPlain.Location = New System.Drawing.Point(123, 30)
        Me.txtPlain.Name = "txtPlain"
        Me.txtPlain.Size = New System.Drawing.Size(584, 22)
        Me.txtPlain.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Plain Text:"
        '
        'txtEnc
        '
        Me.txtEnc.Location = New System.Drawing.Point(123, 58)
        Me.txtEnc.Name = "txtEnc"
        Me.txtEnc.Size = New System.Drawing.Size(584, 22)
        Me.txtEnc.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(30, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Enc Text:"
        '
        'txtDec
        '
        Me.txtDec.Location = New System.Drawing.Point(123, 86)
        Me.txtDec.Name = "txtDec"
        Me.txtDec.Size = New System.Drawing.Size(584, 22)
        Me.txtDec.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(30, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Dec Text:"
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(257, 142)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(128, 39)
        Me.btnClear.TabIndex = 7
        Me.btnClear.Text = "Reset"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'txtKEY
        '
        Me.txtKEY.Location = New System.Drawing.Point(123, 114)
        Me.txtKEY.Name = "txtKEY"
        Me.txtKEY.Size = New System.Drawing.Size(584, 22)
        Me.txtKEY.TabIndex = 8
        Me.txtKEY.Text = "KALAUAKUKAYA.HARIHARI.HARIRAYA"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(30, 119)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 17)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "KEY:"
        '
        'btnCopyEnc
        '
        Me.btnCopyEnc.Location = New System.Drawing.Point(391, 142)
        Me.btnCopyEnc.Name = "btnCopyEnc"
        Me.btnCopyEnc.Size = New System.Drawing.Size(162, 39)
        Me.btnCopyEnc.TabIndex = 10
        Me.btnCopyEnc.Text = "Copy Encrypted String"
        Me.btnCopyEnc.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblStatus.Location = New System.Drawing.Point(30, 190)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(653, 23)
        Me.lblStatus.TabIndex = 11
        Me.lblStatus.Text = "Ready."
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(735, 222)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.btnCopyEnc)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtKEY)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDec)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtEnc)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPlain)
        Me.Controls.Add(Me.btnProc)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AESEncryptor"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnProc As System.Windows.Forms.Button
    Friend WithEvents txtPlain As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtEnc As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDec As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents txtKEY As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnCopyEnc As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label

End Class
