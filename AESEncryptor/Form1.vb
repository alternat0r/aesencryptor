﻿Public Class Form1

    ' a function to encrypt the given string. Two parameter with input and key. This function return result as string.
    Public Function AES_Encrypt(ByVal input As String, ByVal pass As String) As String
        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim encrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
            Array.Copy(temp, 0, hash, 0, 16)
            Array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = System.Security.Cryptography.CipherMode.ECB
            Dim DESEncrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateEncryptor
            Dim Buffer As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(input)
            encrypted = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
            Return encrypted
        Catch ex As Exception
        End Try

    End Function

    ' a function to decrypt the given string. Two parameter with input and key. This function return result as string.
    Public Function AES_Decrypt(ByVal input As String, ByVal pass As String) As String
        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim decrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
            Array.Copy(temp, 0, hash, 0, 16)
            Array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = System.Security.Cryptography.CipherMode.ECB
            Dim DESDecrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateDecryptor
            Dim Buffer As Byte() = Convert.FromBase64String(input)
            decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
            Return decrypted
        Catch ex As Exception
        End Try
    End Function

    ' process button to determind user input whether to encrypt or decrypt.
    Private Sub btnProc_Click(sender As Object, e As EventArgs) Handles btnProc.Click
        If txtEnc.TextLength > 0 Then
            txtDec.Text = AES_Decrypt(txtEnc.Text, txtKEY.Text)
            lblStatus.Text = "Decrypt done."
        Else
            If txtPlain.Text = "" Then
                lblStatus.Text = "Please insert text string on Plain Text box."
            Else
                txtEnc.Text = AES_Encrypt(txtPlain.Text, txtKEY.Text)
                txtDec.Text = AES_Decrypt(txtEnc.Text, txtKEY.Text)
                lblStatus.Text = "Done."
            End If
        End If
    End Sub

    ' Reset button. Clear input and reset to default setting.
    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtDec.Text = ""
        txtEnc.Text = ""
        txtPlain.Text = ""
        txtKEY.Text = "KALAUAKUKAYA.HARIHARI.HARIRAYA"
        lblStatus.Text = "Ready."
    End Sub

    ' copy encrypted string into memory/clipboard.
    Private Sub btnCopyEnc_Click(sender As Object, e As EventArgs) Handles btnCopyEnc.Click
        If txtEnc.Text = "" Then
            lblStatus.Text = "Please insert text string on Plain Text box."
        Else
            Clipboard.SetText(txtEnc.Text)
            lblStatus.Text = "Copied to clipboard."
        End If
    End Sub
End Class
